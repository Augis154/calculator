comp:
	g++ -o calc -O3 -W --std=c++14 calculator.cpp
	./calc 25+63*7

run:
	./calc 2+2*2

cpy:
	sudo cp calc /usr/bin

install: comp
	make cpy
