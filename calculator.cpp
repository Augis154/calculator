
#include "Calculator.h"

Calculator MyCalculator;

int main(int argc, char* argv[]) {
    MyCalculator.test();
    string Str;

    if (argc == 1) {
        // cout << "Usage: ./calculator \"expression\"" << "\n";
        // cout << "Example: \"./calculator 3 + 4*5\"" << "\n";

        while(true) {
            cout << "> ";
            cin >> Str;
            cout << MyCalculator.eval(Str) << endl;
        }
    } else {
        string expression = "";

        for (int i = 1; i < argc; ++i)
            expression += string(argv[i]);

        //cerr << "Evaluating expression: " << expression << "\n";
        cout << MyCalculator.eval(expression) << "\n";
    }

    return 0;
}
